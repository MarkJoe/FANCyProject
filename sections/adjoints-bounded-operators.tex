\section{Adjoints of bounded operators}
\label{sec:adjoints-bounded-operators}

\para 
As before, the symbols $\hilbertH$ and $\hilbertH_k$ with $k=1,2$ always stand for
Hilbert spaces over the field $\fldK$ of real or complex numbers.
Several results of this section hold only in the complex case, thouhgh. Therefore we will be
quite precise in stating all necessary assumptions, in particular about the ground field. 

Let $A \in \blinOps (\hilbertH_1,\hilbertH_2)$ that is let 
$A:\hilbertH_1 \rightarrow \hilbertH_2$ be linear and bounded. Then the map
\[
  b_A : \hilbertH_1 \times \hilbertH_2 \to \fldK, \: (v,w) \mapsto \inprod{Av, w} 
\]
is sesquilinear and bounded with norm
\[
\norm{b_A} = \sup \big\{ \left| b_A(v,w)\right| \bigmid
   v \in \hilbertH_1, \:  w \in \hilbertH_2, \: \norm{w}=\norm{v}=1 \big\} = 
                \norm{A} \ .
\]
By \Cref{thm:correspondence-bounded-sesquilinear-forms-bounded-operators} 
to the Riesz representation theorem there exists a unique bounded linear operator  
$A^* : \hilbertH_2 \to \hilbertH_1$ such that
\[
 b_A (v,w) = \inprod{ v, A^* w} \quad 
 \text{for all } v \in \hilbertH_1, \: w\in \hilbertH_2 \ .
\]
This operator satisfies
\begin{equation}
  \label{eq:equality-norm-operator-adjoint}
  \norm{A^*}= \norm{b_A} = \norm{A}\ .
\end{equation}

\begin{definition}
The unique operator $A^*\in \blinOps (\hilbertH_2,\hilbertH_1)$ associated to 
an operator $A\in \blinOps(\hilbertH_1,\hilbertH_2)$ such that 
\[
 \inprod{Av,w} = \inprod{ v, A^*w} \quad 
 \text{for all } v \in \hilbertH_1, \: w\in \hilbertH_2 
\]
is called the \emph{adjoint} of $A$.
\end{definition}

The fundamental property of the adjoint operation is given by the following result.


\begin{proposition}\label{thm:adjoint-operation-conjugate-linear-map-square-identity}
  The adjoint map ${}^*: \blinOps(\hilbertH_1,\hilbertH_2) \to\blinOps(\hilbertH_2,\hilbertH_1)$
  is a conjugate linear isometry whose square coincides with  the identity operation that is
  $A^{**} = A$ for all $A \in \blinOps(\hilbertH_1,\hilbertH_2)$.
\end{proposition}

\begin{proof}
  By the proof of \Cref{thm:correspondence-bounded-sesquilinear-forms-bounded-operators}, 
  $A^* w = \langle w, A( - )\rangle^\sharp$ for all $w\in \hilbertH_2$. Since the inner product is linear
  in the second argument and the operator ${}^\sharp$ conjugate linear, the map
  $A \mapsto A^*$ is conjugate linear in $A$. 
  By Equation \eqref{eq:equality-norm-operator-adjoint}, the adjoint map is an isometry. 
  The relation $A^{**}=A$ follows by uniqueness of the adjoint and since
  \[
   \inprod{A^*w, v} = \inprod{ w, Av} \quad 
   \text{for all } v \in \hilbertH_1, \: w\in \hilbertH_2 \ . 
  \]
\end{proof}

\begin{definition}
  An operator $A \in \blinOps (\hilbertH)$ is called \emph{self-adjoint} if $A = A^*$,
  \emph{unitary} if $A^* = A^{-1}$, and \emph{normal} if $[A, A^*] := AA^* - A^*A = 0$.
\end{definition}


We note that self-adjoint and unitary operators are always normal, but normal operators do not have to
be self-adjoint or unitary. In the remainder of this section, we gather several results on self-adjoint
and normal operators.

\begin{proposition}\label{thm:selfadjointness-criterion-operator-complex-hilbert-space}
  Assume that the ground field $\fldK$ of the Hilbert space $\hilbertH$ is the field of complex numbers.
  An operator $A \in \blinOps (\hilbertH)$ then is self-adjoint if and only if $\inprod{Av, v} \in \R$
  for all $v \in \hilbertH$.
\end{proposition}

\begin{proof}
($\Rightarrow$) If $A$ is self-adjoint, then
\[
\inprod{Av, v} = \inprod{v, A^*v} = \inprod{v, Av} = \overline{\inprod{Av,v}}\ ,
\]
which implies that $\inprod{Av,v} \in \R$.

($\Leftarrow$) Suppose that $\inprod{Av, v} \in \R$ for all $v \in \hilbertH$. We know
\begin{equation}
  \label{eq:expansion-inner-product-operator-action-argument}
  \inprod{A(v+w), v+w} = \inprod{Av, v} + \inprod{Av, w} + \inprod{Aw, v} + \inprod{Aw, w} \ .
\end{equation}
By assumption, $\inprod{A(v+w), v+w}$, $\inprod{Av, v}$, and $\inprod{Aw, w}$ are all real. This implies
that the sum $\inprod{Av, w} + \inprod{Aw, v}$ is real as well, so
\[
  \Im \, \inprod{Av, w} = -\Im \, \inprod{Aw, v}= \Im \, \inprod{v, Aw} \ .
\]
Since this holds for all $w \in \hilbertH$, it holds for $\cplxi w$, too. Thus,
\[
  \Re \, \inprod{Av, w} = \Im \, \cplxi \inprod{Av, w} = \Im \, \inprod{Av, \cplxi w} = \Im \, \inprod{v, A( \cplxi w)} =
  \Im \, \cplxi \inprod{v, Aw}  = \Re \,\inprod{v, Aw} \ .
\]
Combining the above two lines yields $\inprod{Av, w} = \inprod{v, Aw}$ for all $v, w \in \hilbertH$.
By uniqueness of the adjoint this implies that $A = A^*$.
\end{proof}

\begin{proposition}
  Assume that the ground field $\fldK$ of the Hilbert space $\hilbertH$ is the field of
  complex numbers and let $A \in \blinOps (\hilbertH)$.
  If $\inprod{Av, v} = 0$ holds for all $v \in \hilbertH$, then $A = 0$.
\end{proposition}

\begin{proof}
  Since $\inprod{Av, v} = 0$ for all $v \in H$, equation
  \eqref{eq:expansion-inner-product-operator-action-argument} from the proof of
  \Cref{thm:selfadjointness-criterion-operator-complex-hilbert-space} reduces to
\[
  \inprod{Av, w} = -\inprod{Aw, v} = -\inprod{w, Av} = -\overline{\inprod{Av, w}} \quad
  \text{for all } v, w \in \hilbertH \ .
\]
That means that $\inprod{Av,w}$ has no real part for all $v, w \in \hilbertH$. But then fixing $v$ and
setting $w = Av$ implies $\| Av \|^2 = 0$ for all $v \in \hilbertH$, so $A = 0$.
\end{proof}

\begin{example}
  The preceding proposition does not hold in the real case. To see this
  take rotation by $\frac \pi 2$:
  \[
    R = 
    \begin{pmatrix}
      \cos \frac \pi2 &  -\sin  \frac \pi2 \\
      \sin  \frac \pi2 & \cos \frac \pi2
    \end{pmatrix} 
  \]
  Then $\inprod{Rv,v} =0$ for all $v\in \R^2$, but $R$ is non-zero. Note that
  the example of the rotation operator $R$ also shows that the criterion for self-adjointness
  from \Cref{thm:selfadjointness-criterion-operator-complex-hilbert-space} can not be applied in the real case. 
\end{example}

\begin{lemma}[cf.~{\cite[Lem.\ 22.4]{HirScharEF}}]\label{thm:estimate-sum-inner-products-operator-action-vector}
  Assume that $A$ is a bounded linear operator on the real or complex Hilbert space $\hilbertH$
  for which there exists a $C\geq 0$ such that
  \begin{equation*}
     |\inprod{Av,v}| \leq C \| v\|^2 \quad \text{for all } v \in \hilbertH \ .
  \end{equation*}
  Then 
  \begin{equation}\label{eq:estimate-sum-inner-products-operator-action-vector-real-case}
     |\inprod{ Av,w} + \inprod{v,Aw} | \leq 2 C \| v\| \| w\| \quad \text{for all } v,w \in \hilbertH \ .
  \end{equation}
  In case $\hilbertH$ is a complex Hilbert space one even has the sharper estimate
  \begin{equation}\label{eq:estimate-sum-inner-products-operator-action-vector-complex-case}
     |\inprod{ Av,w}| + | \inprod{v,Aw} | \leq 2 C \| v\| \| w\| \quad \text{for all } v,w \in \hilbertH \ .
  \end{equation}  
\end{lemma}

\begin{proof}
  We start with the equality
  \begin{equation}
  \label{eq:expansion-inner-product-operator-action-sum-difference-arguments}
  \inprod{A(v+w), v+w} + \inprod{A(v-w), v-w}= 2 (\inprod{Av, w} + \inprod{Aw, v}) \ .
  % \quad \text{for all } v,w\in\hilbertH \ .
  \end{equation}
   By assumption and the parallelogram identity \eqref{eq:parallelogram-identity} this entails
   \begin{equation}
   \label{eq:estimate-sum-inner-products-operator-action-vector}   
    2 | \inprod{Av, w} + \inprod{Aw, v}|  \leq  C \left( \| v+w\|^2 + \| v-w\|^2 \right)
    = 2 C \big( \| v\|^2 + \| w\|^2 \big) \ .
    % \quad \text{for all } v,w\in\hilbertH \ .
  \end{equation}
  The claim obviously holds for $v=0$ or $w=0$, so we assume from now on that both $v$ and $w$ are non-zero.
  Then put $a= \sqrt{\frac{\|v\|}{\|w\|}}$ and replace in
  \eqref{eq:estimate-sum-inner-products-operator-action-vector} $v$ by $\frac{v}{a}$ and
  $w$ by $aw$. One obtains
  \[
    | \inprod{Av, w} + \inprod{Aw, v}|   \leq    C \left( \Big\|\frac{v}{a} \Big\|^2 + \Big\| a w \Big\|^2 \right)
    = 2 C   \| v \|  \|  w \| 
  \]
  which is the claim in the real case. If $\hilbertH$ is a complex Hilbert space, let $x,y$ be complex numbers
  of modulus $1$. In the just proven estimate multiply the left side with $|x|$ and replace $w$ with $yw$.
  This gives
  \begin{equation}
   \label{eq:estimate-sum-inner-products-operator-action-vector-added-factors}
    \big| xy \inprod{Av, w} + x \overline{y} \inprod{Aw, v} \big| =
    |x| \cdot | \inprod{Av, yw} + \inprod{A(yw), v}| 
    \leq  2 C   \| v \|  \|  w \| \ .
  \end{equation}
  Now write $ \inprod{Av, w} = r e^{\cplxi \varphi}$ and $\inprod{Aw, v} = s e^{\cplxi \psi}$
  with $r,s\geq 0$ and $\varphi,\psi \in \R$. Then put
  \[
    x = e^{-\cplxi \frac 12 (\varphi +\psi))} \quad \text{and} \quad y =  e^{-\cplxi \frac 12 (\varphi - \psi))} \ .
  \]
  With these values,  \eqref{eq:estimate-sum-inner-products-operator-action-vector-added-factors}   becomes
  \[
     |\inprod{ Av,w}| + | \inprod{v,Aw} | \leq 2 C \| v\| \| w\|
  \]
  which was to be shown. 
\end{proof}

\begin{proposition}
  If $\hilbertH$ is a Hilbert space over the field $\fldK$ of real or complex numbers
  and $A \in \blinOps (\hilbertH)$ is self-adjoint, then 
  \[
    \norm{A} = \sup_{\norm{v} = 1} |\inprod{Av, v}| \ .
  \]
\end{proposition}

\begin{proof}
We know
\begin{equation}
  \label{eq:operator-norm-inner-product-representation}
  \norm{A} = \sup_{\norm{v} = \norm{w} = 1} | \inprod{Av, w}| \ ,
\end{equation}
so we clearly have
\[
\sup_{\norm{v} = 1} \abs{\inprod{Av, v}} \leq \norm{A} \ .
\]
The other direction follows from Equation \eqref{eq:operator-norm-inner-product-representation}
and \Cref{thm:estimate-sum-inner-products-operator-action-vector} since $A$ is self-adjoint. 
\end{proof}

\begin{proposition}
  If $\hilbertH$ is a real or complex Hilbert space and $A \in \blinOps (\hilbertH)$, then $A^*A$ is self-adjoint
  and $\norm{A^*A} = \norm{A}^2$. 
\end{proposition}

\begin{proof}
For arbitrary $v,w \in \hilbertH$, we have
\[
\inprod{A^*Av, w} = \inprod{Av, Aw} = \inprod{v, A^*Aw} 
\]
so $A^*A$ is self-adjoint. Then
\[
  \norm{A^*A} = \sup_{\norm{v} = \norm{w} = 1} \abs{\inprod{A^*Av, w}}  =
  \sup_{\norm{v} = \norm{w} = 1} \abs{\inprod{Av, Aw}} = \norm{A}^2 \ ,
\]
where the last equality is a consequence of the Cauchy--Schwarz inequality and the observation that
for all $\varepsilon >0$ there exists a unit vector $v$ such that $ \inprod{Av, Av} \geq \norm{A}^2 -\varepsilon$. 
\end{proof}

\begin{proposition}
  Let $\hilbertH$ be a complex Hilbert space $\hilbertH$. 
  If $A \in \blinOps (\hilbertH)$, then there exist unique self-adjoint $B,C \in \blinOps (\hilbertH)$
  such that $A = B+\cplxi C$. Furthermore, $A$ is normal if and only if $[B,C] = 0$.
\end{proposition}

\begin{proof}
We define
\[
B = \frac{1}{2}(A + A^*) \quad \text{ and } \quad C = \frac{\cplxi}{2}(A^* - A).
\]
Clearly $A = B +\!\cplxi C$. Note also that $A^* = B - \cplxi C$. Furthermore,
by \Cref{thm:adjoint-operation-conjugate-linear-map-square-identity}
\[
  B^* = \frac{1}{2}(A^* + A) = B 
\]
and
\[
  C^* = - \frac{\cplxi}{2}(A - A^* ) = C \ .
\]
Hence $B$ and $C$ are self-adjoint, so fulfill the claim. Let us show uniqueness.
Assume that $B^\prime,C^\prime \in \blinOps (\hilbertH)$
are selfadjoint and satisfy $A = {B^\prime} +\!\cplxi {C^\prime}$. Then
\[
  B - {B^\prime} = B^* - {B^\prime}^* = \left( \cplxi ({C^\prime} - C)\right)^* =
  -  \cplxi  ({C^\prime} - C) = - ( B - {B^\prime}) \ .
\]  
Hence $B = {B^\prime}$ and consequently $C = {C^\prime}$. 
Finally, we compute
\[
  [A, A^*] = [B +\!\cplxi  C, B - \cplxi C] = -\cplxi [B, C]+\!\cplxi [C,B] = -2 \cplxi [B,C] \ .
\]
This entails that $A$ is normal if and only if $[B,C] = 0$. 
\end{proof}

\begin{proposition}
  If $A$ is a normal operator on a real or complex Hilbert space $\hilbertH$,
  then
  \[
    \norm{Av} = \norm{A^*v} \quad\text{for all }v \in \hilbertH \ .
  \]  
\end{proposition}

\begin{proof}
Using the fact that $A^*A = AA^*$, we compute
\[
\norm{Av}^2 = \inprod{Av, Av} = \inprod{v, A^*Av} = \inprod{v, AA^*v} = \inprod{A^*v, A^*v} = \norm{A^*v}^2 \ .
\]
Taking the square root yields the desired result.
\end{proof}
